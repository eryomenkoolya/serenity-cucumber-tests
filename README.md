# Sample Serenity API tests using Cucumber

## Introduction

This is a sample Rest API test solution for endpoints available in https://any-api.com/dataatwork_org/dataatwork_org/docs/API_Description. API provides complete and standard data store for canonical and emerging skills, knowledge, abilities, tools, technologies, and how they are related to jobs.

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber and Maven.

## Framework & Design Considerations
- Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features.
- API calls are made using RestAssured and SerenityRest which is a wrapper on top of RestAssured.
- Tests are written in BDD Gherkin format in Cucumber feature files and it is represented as a living documentation in the test report.
- Steps libraries are stored in the separate package and splitted based on the domain model. Each class in the package consist of API calls/actions specific to the certain domain.
- Steps libraries are called in a step definitions classes which are in-turn called from BDD tests.

### The project directory structure

```Gherkin
src
  + test
    + java                            Test runners and supporting code
      + models                        Package with domain models
          Job                         Domain model with correspondent fields and mapping to json fields in the response 
      + steps                         Package with all API calls/actions
          BaseApiSteps                General API calls/actions across all the domain models
          CommonSpec                  Common Request and Response Spec for the API calls
          JobApiSteps                 API calls/actions specific to the certain domain (job, skill)
      + stepdefinitions               Package with step definitions for BDD scenarios
          JobStepDefinitions          Mappings of BDD scenarios and methods in Java classes
      + CucumberTestSuite             Test runner
    + resources
      + features                      Feature files directory
          jobs_search_by_id.feature   Feature file containing BDD scenarios
serenity.properties                   Configurations file
```
## Executing the tests
Run `mvn clean verify` from the command line.

The test results will be recorded here `target/site/serenity/index.html`.

### Additional configurations

Additional command line parameter can be passed for switching the service base url.
```
mvn clean verify -Drestapi.baseurl=http://api.dataatwork.org/v1
```
The service base url is set in the `serenity.properties` file. 
