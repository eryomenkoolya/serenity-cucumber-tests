package serenity.sample.models;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Job {

    private String uuid;
    private String suggestion;
    @SerializedName("normalized_job_title")
    private String normalizedJobTitle;
    @SerializedName("parent_uuid")
    private String parentUuid;
}
