package serenity.sample.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import serenity.sample.steps.JobApiSteps;
import serenity.sample.models.Job;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class JobStepDefinitions {

    @Steps
    JobApiSteps jobApiSteps;

    @When("I send GET \\/jobs\\/autocomplete with query params:")
    public void i_send_get_jobs_autocomplete_with_query_params(DataTable table) {
        Map<String, String> queryParams = new HashMap<>(table.asMap(String.class, String.class));

        for (Map.Entry<String, String> data : queryParams.entrySet()) {

            queryParams.put(data.getKey(), data.getValue());
        }
        jobApiSteps.getJobsAutocomplete(queryParams);
    }

    @When("I send GET \\/jobs\\/autocomplete without query params")
    public void i_send_get_jobs_autocomplete_without_query_params() {
        jobApiSteps.getJobsAutocomplete();
    }

    @Then("I should get response status code {int}")
    public void i_should_get_response_status_code(int expectedStatusCode) {
        assertThat(jobApiSteps.getStatusCode()).isEqualTo(expectedStatusCode);
    }

    @Then("I should get jobs which begin with {string}")
    public void i_should_get_jobs_which_begin_with(String word) {
        Assertions.assertThat(jobApiSteps.getJobsResponse()).extracting(Job::getNormalizedJobTitle).allMatch(value -> value.startsWith(word));
    }

    @Then("I should get jobs which end with {string}")
    public void i_should_get_jobs_which_end_with(String word) {
        Assertions.assertThat(jobApiSteps.getJobsResponse()).extracting(Job::getNormalizedJobTitle).allMatch(value -> value.endsWith(word));
    }

    @Then("I should get jobs which contain {string}")
    public void i_should_get_jobs_which_contain(String word) {
        Assertions.assertThat(jobApiSteps.getJobsResponse()).extracting(Job::getNormalizedJobTitle).allMatch(value -> value.contains(word));
    }

    @Given("I note id of any found job")
    public void i_note_id_of_any_found_job() {
        jobApiSteps.getRandomJobIdFromResponse();
    }

    @When("I send GET \\/jobs\\/id with noted id")
    public void i_send_get_jobs_id_with_noted_id() {
        String jobId = Serenity.sessionVariableCalled("randomJobId").toString();
        jobApiSteps.getJobById(jobId);
    }

    @When("I send GET \\/jobs\\/id with non-existent id")
    public void i_send_get_jobs_id_with_non_existent_id() {
        jobApiSteps.getJobById(UUID.randomUUID().toString());
    }
}
