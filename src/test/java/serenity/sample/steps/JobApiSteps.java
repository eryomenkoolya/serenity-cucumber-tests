package serenity.sample.steps;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import serenity.sample.models.Job;

import java.util.Map;

public class JobApiSteps extends BaseApiSteps {

    public static final String JOB_AUTOCOMPLETE_PATH = "/jobs/autocomplete";
    public static final String JOB_PATH = "/jobs";

    @Step
    public Response getJobsAutocomplete(Map<String, String> queryParams) {
        return SerenityRest.given()
                .queryParams(queryParams)
                .when().get(JOB_AUTOCOMPLETE_PATH);
    }

    @Step
    public Response getJobsAutocomplete() {
        return SerenityRest.given()
                .when().get(JOB_AUTOCOMPLETE_PATH);
    }

    @Step
    public Response getJobById(String jobId) {
        return SerenityRest.given()
                .pathParam("id", jobId)
                .when().get(JOB_PATH + "/{id}");
    }

    @Step
    public Job[] getJobsResponse(){
        return SerenityRest.then().extract().as(Job[].class);
    }

    @Step
    public String getRandomJobIdFromResponse() {
        Job[] jobResponse = SerenityRest.then().extract().response().as(Job[].class);
        int randomIndex = (int) (Math.random() * jobResponse.length);
        String randomJobId = String.valueOf(jobResponse[randomIndex].getUuid());
        Serenity.setSessionVariable("randomJobId").to(randomJobId);
        return randomJobId;
    }
}
