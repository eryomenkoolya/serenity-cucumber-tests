package serenity.sample.steps;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class BaseApiSteps extends CommonSpec {

    @Step
    public int getStatusCode() {
        return SerenityRest.then().extract().statusCode();
    }
}
