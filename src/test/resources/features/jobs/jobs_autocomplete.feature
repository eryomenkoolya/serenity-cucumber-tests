Feature: Get jobs via autocomplete
  As a user
  I should be able to retrieve jobs via autocomplete

  Scenario: Get jobs via autocomplete specifying existing 'begins_with' search parameter
    When I send GET /jobs/autocomplete with query params:
      | begins_with | software |
    Then I should get response status code 200
    And I should get jobs which begin with 'software'

  Scenario: Get jobs via autocomplete specifying existing 'ends_with' search parameter
    When I send GET /jobs/autocomplete with query params:
      | ends_with | engineer |
    Then I should get response status code 200
    And I should get jobs which end with 'engineer'

  Scenario: Get jobs via autocomplete specifying existing 'contains' search parameter
    When I send GET /jobs/autocomplete with query params:
      | contains | engineer |
    Then I should get response status code 200
    And I should get jobs which contain 'engineer'

  Scenario: Get jobs via autocomplete specifying existing 'contains' search parameter with non-existent value
    When I send GET /jobs/autocomplete with query params:
      | contains | sfkhsleihfl |
    Then I should get response status code 404

  Scenario: Get jobs via autocomplete specifying non-existent search parameter
    When I send GET /jobs/autocomplete with query params:
      | begins | software |
    Then I should get response status code 400

  Scenario: Get jobs via autocomplete without specifying search parameter
    When I send GET /jobs/autocomplete without query params
    Then I should get response status code 400
