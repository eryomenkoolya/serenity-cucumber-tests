Feature: Get job by id
  As a user
  I should be able to retrieve job by id

  Scenario: Get job by existing id
    Given I send GET /jobs/autocomplete with query params:
      | contains | teacher |
    And I note id of any found job
    When I send GET /jobs/id with noted id
    Then I should get response status code 200

  Scenario: Get job by non-existent id
    When I send GET /jobs/id with non-existent id
    Then I should get response status code 404